import React from "react";
import "./App.css";
import Module from "./Module";
import Switcher from "./Switcher";

function App() {
  return (
    <div className="App">
      <Switcher />
      <Module />
    </div>
  );
}

export default App;
