import _ from 'lodash';

function getModuleName(module) {
  return _.get(module, 'data.name');
}

export default {
  getModuleName,
}
