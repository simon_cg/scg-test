import React from "react";
import Headphones from "./Headphones";
import PlayButton from "./PlayButton";
import selectors from "./selectors/module";

const moduleData = require("./data/module-data.json");
const userData = require("./data/user-data.json");

export default function Module() {

  const [visible, setVisible] = React.useState([1, 2, 3])
  const [lastDone, setLastDone] = React.useState("")

  const { levels } = moduleData.data
  const { completedExercises } = userData

  const originalElement = (<><div>
    <div className="moduleTitle">{selectors.getModuleName(moduleData)}</div>
  </div>
    <div className="moduleContent">
      <div>
        <div
          className="levelTitle"
          onClick={() => {
            console.log("Level 1 clicked");
          }}
        >
          Level 1: Getting Started
      </div>
      </div>
      <div className="exercise">
        <div className="exerciseTitle">First Exercise</div>
        <div className="exerciseContent">
          <Headphones />
        </div>
      </div>
      <div className="exercise">
        <div className="exerciseTitle">Second Exercise</div>
        <div className="exerciseContent">
          <PlayButton />
        </div>
      </div>
      <div className="exercise">
        <div className="exerciseTitle">Third Exercise</div>
        <div className="exerciseContent">
          <Headphones />
        </div>
      </div>
      <div>
        <div className="levelTitle">Level 2: Next Steps</div>
      </div>
      <div className="exercise">
        <div className="exerciseTitle">Second Level Exercise</div>
        <div className="exerciseContent">
          <Headphones />
        </div>
      </div>
      <div className="exercise">
        <div className="exerciseTitle">Almost There</div>
        <div className="exerciseContent">
          <PlayButton />
        </div>
      </div>
      <div className="exercise">
        <div className="exerciseTitle">Final Exercise</div>
        <div className="exerciseContent">
          <PlayButton />
        </div>
      </div>
    </div></>)

  const handleToggle = ((id) => {
    if (visible.includes(id)) {
      const newVisible = visible.filter(data => data !== id)
      setVisible(newVisible)
    } else {
      const newVisible = id
      setVisible(oldVisible => [...oldVisible, newVisible])
    }
  })

  // this is not the correct solution nor the correct implementation for Task 3 but I'm leaving it here for now.
  React.useEffect(() => {
    completedExercises.map((completed) => {
      return handleToggle(completed.exerciseId)
    })
  }, [completedExercises])

  // Task 4
  const exerciseTiming = () => {
    const sortedComplete = completedExercises
    sortedComplete.sort()
    const lastExercise = sortedComplete[0].exerciseId

    levels.map((level) => {

      const [exercise] = level.exercises;

      if (exercise.id === lastExercise) {
        const name = exercise.name

        setLastDone(name)
      } else {
        return null
      }
      return null
    })

    return
  }

  React.useEffect(() => {
    exerciseTiming()
  }, [])

  return (
    <div className="moduleContainer">
      <div>
        <div className="moduleTitle">{selectors.getModuleName(moduleData)}</div>
        <div className="exerciseTitle">Last Exercise Completed: {lastDone}</div>

      </div>

      {levels ? levels.map((level) => {

        return (
          <div key={level.id}>
            <div className="moduleContent">
              <div>
                <div
                  className="levelTitle"
                  onClick={() => {
                    // console.log(`Level ${level.id} clicked`);
                    handleToggle(level.id)
                  }}
                  style={{ cursor: "pointer" }}
                >
                  Level {level.id}: {level.name}
                </div>
              </div>

              {visible.includes(level.id) && level.exercises.map((exercise) => (
                <div className="exercise" key={exercise.id}>
                  <div className="exerciseTitle">Exercise {exercise.id}: {exercise.name}</div>
                  <div className="exerciseContent">
                    {exercise.audioUrl ? <Headphones /> : <PlayButton />}
                  </div>
                </div>
              ))}

            </div>
          </div>)
      }) : originalElement}
    </div>
  );
}

// const [visible, setVisible] = React.useState({ levels: [{ id: 1, toggle: { show: true } }, { id: 2, toggle: { show: true } }, { id: 2, toggle: { show: true } }] });

  // const toggleVisible = (id) => {
  //   console.log(id);
  //   let updatedLevels = visible.levels.map(level => {
  //     if (level.id === id) {
  //       return { ...level, toggle: !level.toggle };
  //     }
  //     return level;
  //   });

  //   setVisible({ levels: updatedLevels });
  // }