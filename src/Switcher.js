import React from "react";

// This component is designed to animate between three inputs, showing one at
// a time and going to the next input when the "Next" button is pressed.

// When on the last input the button text should change to ‘Submit’

// and pressing the button should console log the values of the inputs and not animate further.

// The component should focus the active input as it comes into view.

export default function Switcher() {
  const [currentStage, setCurrentStage] = React.useState(1);

  let buttonElement = <button onClick={() => move("forwards")}>Next</button>

  if (currentStage === 3) {
    buttonElement = <button onClick={() => console.log("[inputs]", inputs)}>Submit</button>
  }

  const inputs = React.useRef({
    1: null,
    2: null,
    3: null,
  });

  React.useEffect(() => {
    inputs.current[currentStage].focus();
  }, [currentStage]);

  function move(direction) {

    if (direction === "forwards") {
      setCurrentStage(currentStage + 1);
    }

    const input = inputs.current[currentStage];
    if (input) {
      inputs.current[currentStage].focus();
    }
  }


  return (
    <div className="switcherContainer">
      <div className="switcherItems">
        <div className={`switcherItemsInner stage${currentStage}`}>
          <div className="switcherItem" style={{ backgroundColor: "red" }}>
            <input ref={r => inputs.current[1] = r} type="number" />
          </div>
          <div className="switcherItem" style={{ backgroundColor: "green" }}>
            <input ref={r => inputs.current[2] = r} type="number" />
          </div>
          <div className="switcherItem" style={{ backgroundColor: "blue" }}>
            <input ref={r => inputs.current[3] = r} type="number" />
          </div>
        </div>
      </div>
      {buttonElement}
    </div>
  );
}
